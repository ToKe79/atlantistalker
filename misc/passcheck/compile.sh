#!/bin/sh
CCACHE=$(which ccache)
GCC=$(which gcc)
CC="$CCACHE $GCC"
PROJECT="passcheck"
LIBS="-lmysqlclient -ldl -rdynamic $(xml2-config --libs)"
CFLAGS="-Wall -Wextra -Wformat=2 -ggdb -pedantic -I. $(xml2-config --cflags)"
IN="passcheck.c"

if [ "${HAVE_CONFIG}" = "1" ] ; then
	CFLAGS="-DHAVE_CONFIG ${CFLAGS}"
fi

echo "Compiling '${PROJECT}'..."
[ -z "${GCC}" ] && { echo "Error: gcc not found!" ; exit 1 ; }
${CC} ${CFLAGS} ${IN} -o ${PROJECT} ${LIBS}
ret=$?
[ $ret -eq 0 ] && echo "Finished!"
exit $ret
