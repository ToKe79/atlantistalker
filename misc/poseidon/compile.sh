#!/bin/sh

if [ -n "$1" ] ; then
	if [ "$1" = "help" ] ; then
		echo -e "Usage:\n  $0 [binary]\n"
		exit 0
	fi
	binary="$1"
else
	binary="poseidon"
fi

if [ ! -f "poseidon.h" ] ; then
	echo "poseidon.h not found! Copying sample (poseidon-sample.h)"
	cp poseidon-sample.h poseidon.h
fi

CCACHE=$(which ccache)
GCC=$(which gcc)
CC="${CCACHE} ${GCC}"

[ -z "${GCC}" ] && { echo "Error: gcc not found!" ; exit 1 ; }

${CC} -Wall -Wextra -Wformat=2 -pedantic -ggdb -o $binary poseidon.c
ret=$?
[ $ret -eq 0 ] && echo "Finished. You can run the bot by ./$binary"
exit $ret
