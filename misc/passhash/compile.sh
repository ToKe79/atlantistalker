#!/bin/sh
CCACHE=$(which ccache)
GCC=$(which gcc)
CC="$CCACHE $GCC"
PROJECT="passhash"
LIBS="-lmysqlclient -ldl -rdynamic $(xml2-config --libs)"
CFLAGS="-Wall -Wextra -Wformat=2 -ggdb -pedantic -I. $(xml2-config --cflags)"
IN="passhash.c"

echo "Compiling '${PROJECT}'..."
[ -z "${GCC}" ] && { echo "Error: gcc not found!" ; exit 1 ; }
${CC} ${CFLAGS} ${IN} -o ${PROJECT} ${LIBS}
ret=$?
[ $ret -eq 0 ] && echo "Finished!"
exit $ret
