#!/bin/bash
declare -i ret=0
p=$(pwd)
export MOREWARN=1
make -j
ret+=$?
make libs
ret+=$?
cd $p/misc/poseidon
./compile.sh
ret+=$?
cd $p/misc/passcheck
./compile.sh
ret+=$?
cd $p/misc/passhash
./compile.sh
ret+=$?
cd $p/misc/webtel
make
ret+=$?
exit $ret
